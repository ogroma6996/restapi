#Rest-API

GET    /users      -- list of users         -- 200, 204, 500

GET    /users/:id  -- user by id            -- 200, 404, 500

POST   /users/:id  -- create user           -- 204, 4xx, Header Location: url

PUT    /users/:id  -- fully update user     -- 200/204, 400, 404, 500

PATCH  /users/:id  -- partially update user -- 200/204, 400, 404, 500

DELETE /users/:id  -- delete user by id     -- 204, 400, 404

#List code HTTP

#2xx Success

200 -- OK

204 -- No Content

#4xx Client Error

400 -- Bad Request

404 -- Not Found

#5xx Server Error

500 -- Internal Server Error
