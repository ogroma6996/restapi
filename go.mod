module gitlab.com/ogroma6996/restapi

go 1.16

require (
	github.com/BurntSushi/toml v0.4.1 // indirect
	github.com/ilyakaznacheev/cleanenv v1.2.5
	github.com/joho/godotenv v1.4.0 // indirect
	github.com/julienschmidt/httprouter v1.3.0
	github.com/sirupsen/logrus v1.8.1
	go.mongodb.org/mongo-driver v1.7.3
	gopkg.in/yaml.v2 v2.4.0 // indirect
	olympos.io/encoding/edn v0.0.0-20201019073823-d3554ca0b0a3 // indirect
)
