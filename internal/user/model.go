package user

type User struct {
	UUID         string `yaml:"id" bson:"_id, omitempty"`
	Username     string `yaml:"username" bson:"username"`
	PasswordHash string `yaml:"-" bson:"password"`
	Email        string `yaml:"email" bson:"email"`
}

type CreateUserDTO struct {
	Username     string `yaml:"username"`
	PasswordHash string `yaml:"password"`
	Email        string `yaml:"email"`
}
